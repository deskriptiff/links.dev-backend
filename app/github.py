import hashlib
import zlib

import django.core.cache
import requests

from app.constants import CACHE_ERROR_VALUE, MAX_RESPONSE_SIZE


class GithubContentService:

    def __init__(self, cache=None, headers=None):
        self.headers = headers or {"User-Agent": "Mozilla/5.0"}
        self.cache = cache or django.core.cache.caches['default']
        print("GithubContentService initialized")

    def get_page_content(self, username: str, refresh=False):
        url = f"https://raw.githubusercontent.com/{username}/my-links/main/page.json"

        return self.get(url, refresh)

    def get(self, url: str, refresh=False):
        # Check if the cache contains the data for this URL
        cache_key = self.get_cache_key(url)
        data = self.cache.get(cache_key)

        if data is not None and not refresh:
            # Cache hit: return the data from the cache
            print("Cache hit for URL:", url)
            return self.decompress(data)

        # Cache miss: fetch the data from the URL and cache it
        print("Cache miss for URL:", url)
        # Invalidate the cache for that cache_key
        self.cache.delete(cache_key)
        response = requests.get(url, headers=self.headers)
        if not response.ok or response.text == "" or len(response.text) > MAX_RESPONSE_SIZE:
            text = CACHE_ERROR_VALUE
        else:
            text = response.text
        compressed_data = self.compress(text)
        self.cache.set(cache_key, compressed_data, None)

        return text

    def get_css(self, username: str, refresh=False):
        url = f"https://raw.githubusercontent.com/{username}/my-links/main/custom.css"
        return self.get(url, refresh)

    def compress(self, data: str) -> bytes:
        # Compress the string using zlib
        compressed_data = zlib.compress(data.encode())
        return compressed_data

    def decompress(self, data: bytes) -> str:
        # Decompress the data using zlib
        decompressed_data = zlib.decompress(data).decode()
        return decompressed_data

    def get_cache_key(self, url):
        cache_key = hashlib.sha256(url.encode()).hexdigest()
        return cache_key
