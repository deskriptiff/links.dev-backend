# Initialise environment variables
from environ import environ

_env = environ.Env()
environ.Env.read_env()


def env(key, default=None):
    try:
        return _env(key, default=default)
    except:
        return default


def decide_content_type(path):
    if path.endswith('.css'):
        return 'text/css'
    elif path.endswith('.js'):
        return 'application/javascript'
    elif path.endswith('.html'):
        return 'text/html'
    elif path.endswith('.xhtml'):
        return 'application/xhtml+xml'
    elif path.endswith('.jpg'):
        return 'image/jpeg'
    elif path.endswith('.png'):
        return 'image/png'
    elif path.endswith('.gif'):
        return 'image/gif'
    elif path.endswith('.ico'):
        return 'image/x-icon'
    elif path.endswith('.tiff'):
        return 'image/tiff'
    elif path.endswith('.svg'):
        return 'image/svg+xml'
    elif path.endswith('.pdf'):
        return 'application/pdf'
    elif path.endswith('.json'):
        return 'application/json'
    elif path.endswith('.ld+json'):
        return 'application/ld+json'
    elif path.endswith('.xml'):
        return 'application/xml'
    elif path.endswith('.zip'):
        return 'application/zip'
    elif path.endswith('.form'):
        return 'application/x-www-form-urlencoded'
    elif path.endswith('.csv'):
        return 'text/csv'
    return 'text/plain'
