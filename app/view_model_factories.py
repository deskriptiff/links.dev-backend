import json

from .view_models import PageViewModel, LinkViewModel


class PageViewModelFactory:

    @staticmethod
    def from_json_text(json_text: str) -> PageViewModel:
        parsed = json.loads(json_text)
        page_view_model = PageViewModel()
        page_view_model.image_url = parsed.get("image_url", "")
        page_view_model.name = parsed.get("name", "")
        page_view_model.description = parsed.get("description", "")
        page_view_model.links = [LinkViewModelFactory.from_json_text(json.dumps(link)) for link in
                                 parsed.get("links", [])]
        return page_view_model


class LinkViewModelFactory:

    @staticmethod
    def from_json_text(json_text: str) -> LinkViewModel:
        parsed = json.loads(json_text)
        link_view_model = LinkViewModel()
        link_view_model.title = parsed.get("title", "")
        link_view_model.url = parsed.get("url", "")
        link_view_model.icon = parsed.get("icon", "")
        return link_view_model
