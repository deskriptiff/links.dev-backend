ssh -o StrictHostKeyChecking=no -i $SSH_KEY $EC2_USER@$EC2_IP "
  (docker network create --driver bridge create || true) &&
  docker login -u $REGISTRY_USER -p $REGISTRY_PASS &&
  ((docker stop backend || true) && (docker rm backend || true) || true) &&
  docker run --name backend --network=default -d -p 8000:8000 $IMAGE_NAME:$CI_COMMIT_SHA"


export $(grep -v '^#' .env | xargs)